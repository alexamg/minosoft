package de.bixilon.minosoft.game.datatypes;

/**
 * Basically raw chunk stuff. This is not a block like dirt (it can be). Can be dirt, a flower, fire, torch, sapling, fluid (water/lava), nether wart, banner, ...
 */
public interface WorldBlock {
    BlockPosition getBlockPosition();
}
