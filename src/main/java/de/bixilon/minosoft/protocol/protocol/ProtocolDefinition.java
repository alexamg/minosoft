package de.bixilon.minosoft.protocol.protocol;

public final class ProtocolDefinition {
    public static final int STRING_MAX_LEN = 32767;
    public static final int DEFAULT_PORT = 25565;
    public static final int PROTOCOL_PACKET_MAX_SIZE = 2097152;
}
