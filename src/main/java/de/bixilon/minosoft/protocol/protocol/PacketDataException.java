package de.bixilon.minosoft.protocol.protocol;

public class PacketDataException extends Exception {
    public PacketDataException(String message) {
        super(message);
    }
}
